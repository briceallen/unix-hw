/*
 *  Name: Brice Allen
 *  Date: 04/20/2022
 *  Class: CSCI 3800
 *  Description: Homework Four
 *  Status: Compiles and runs on CSE grid
 *  Source(s):
 *
 *             wiki: https://bit.ly/3vAGjDE
 *             die: https://linux.die.net/man/3/datetime
 *             man7: https://man7.org/linux/man-pages/man3/strftime.3.html
 *             gnu: https://www.gnu.org/software/libc/manual/html_node/Date-and-Time.html
 *             The book, slides, and lecture.
 *             Some google/stackoverflow for errors. c is hard.
 *
 *
 *
*/
#include <iomanip>
#include <iostream>
#include <vector>
#include <string>
#include <ctime>

#define dash '-'
#define  mock_data "12/25/2022"

class Now {
private:
    struct tm local_t{};
public:
    Now() {
        time_t t = time(0);
        localtime_r(&t, &local_t);
    }

    std::string getDate(const char *fmt) const {
        char out[1024];
        size_t result = strftime(out, sizeof out, fmt, &local_t);
        return std::string{out,
                           out + result};///Braced initializer avoids repeating the return type from the declaration
    }

    [[nodiscard]] std::string getDateString() const { return getDate("%F %A, %r, %Z "); }
};

class Date {
private:
    int year;
    int month;
    int day;

public:
    __attribute__((unused)) explicit Date(std::string date_string) {
        if (dateValidator(date_string)) {
            year = atoi(&date_string[0]);
            month = atoi(&date_string[5]);
            day = atoi(&date_string[8]);
        } else {
            throw std::invalid_argument("Invalid date");
        }
    }

    [[nodiscard]] int getYear() const {
        return year;
    }

    [[nodiscard]] int getMonth() const {
        return month;
    }

    [[nodiscard]] int getDay() const {
        return day;
    }

    ///
    /// \param date_string
    /// \return
    static inline bool dateValidator(std::string date_string) {
        if (date_string.length() != 10 || date_string[4] != dash || date_string[7] != dash)
            return false;
        if (!isdigit(date_string[0]) || !isdigit(date_string[1]) || !isdigit(date_string[2]) ||
            !isdigit(date_string[3]))
            return false;
        if (!isdigit(date_string[5]) || !isdigit(date_string[6]) || !isdigit(date_string[8]) ||
            !isdigit(date_string[9]))
            return false;

        auto year = atoi(&date_string[0]);
        auto month = atoi(&date_string[5]);
        auto day = atoi(&date_string[8]);
        return !(year <= 0 ||
                 month <= 0 ||
                 month >= 13 ||
                 day <= 0);
    }

    friend std::ostream &operator<<(std::ostream &, Date &);
};

///
/// \param os
/// \param date
/// \return
inline std::ostream &operator<<(std::ostream &os, Date &date) {
    os << std::setfill('0') << std::setw(4) << date.year << '-';
    os << std::setfill('0') << std::setw(2) << date.month << '-';
    os << std::setfill('0') << std::setw(2) << date.day << ':';
    return os;
}

///
/// \param date_one
/// \param date_two
/// \return
inline int diffDays(Date date_one, Date date_two) {
    auto date_one_month = (date_one.getMonth() + 9) % 12;
    auto date_one_year = date_one.getYear() - date_one_month / 10;

    auto date_two_month = (date_two.getMonth() + 9) % 12;
    auto date_two_year = date_two.getYear() - date_two_month / 10;

    auto days1 =
            365 * date_one_year + date_one_year / 4 - date_one_year / 100 + date_one_year / 400 +
            (date_one_month * 306 + 5) / 10 + (date_one.getDay() - 1);
    auto days2 =
            365 * date_two_year + date_two_year / 4 - date_two_year / 100 + date_two_year / 400 +
            (date_two_month * 306 + 5) / 10 + (date_two.getDay() - 1);
    return days2 - days1;
}


//Until the target date: 275 Days, 1 Hours, 39 Minutes, 26 Seconds
//        Total number of seconds of the target date: 23769566 seconds
///
/// \return
int main() {

    Now now_date;

    std::string user_date;

    struct tm tm{};

    std::string temp = now_date.getDate("%Y-%m-%d");
    auto now_storage = temp.data();

    strptime(now_storage, "%Y-%m-%d", &tm);
    time_t t3 = mktime(&tm);
    //        Current date and time: 2022-03-24 Thursday 10:20:34 PM, MDT
    //        Please enter a target date in the future(mm/dd/yyyy): 12/25/2022
    std::cout << "Current date and time: " << now_date.getDateString() << std::endl
              << "Please enter a target date in the future(mm/dd/yyyy):\n >";
    //std::cin >> user_date;
    user_date = mock_data;

    user_date = user_date.substr(6, 4) + '-' + user_date.substr(0, 2) + '-' + user_date.substr(3, 2);
    auto user_date_cstring = user_date.data();
    strptime(user_date_cstring, "%Y-%m-%d", &tm);
    time_t t4 = mktime(&tm);

    if (!Date::dateValidator(user_date)) std::cout << "Invalid date, man!" << std::endl;
    else {
        Date date(user_date_cstring);
        Date date_one(now_storage);
        double days = diffDays(date_one, date);
        auto sec = difftime(t4, t3);
        auto hour = days * 24;
        auto minutes = hour * 60;
        auto seconds = minutes * 60;
        std::cout
                << "Your Date: "
                << date.getYear() << '-' << date.getMonth() << '-' << date.getDay() << '\n'
                << "Difference In: \n"
                << std::fixed << "\tDays : "
                << days << '\n'
                << std::fixed << "\tHours: "
                << hour << '\n'
                << std::fixed << "\tMinutes: "
                << minutes << '\n'
                << std::fixed << "\tSeconds: "
                << seconds << '\n'
                << "\tdifftime() seconds: "
                << sec << std::endl;
    }

    return 0;
}
