# https://hub.docker.com/_/gcc 
FROM gcc:11.2

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y cmake libboost-dev && rm -rf /var/lib/apt/lists/* 

CMD ["cmake"]


